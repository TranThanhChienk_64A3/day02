<?php
date_default_timezone_set('Asia/Ho_Chi_Minh').
$time = date('h:i');
$day = date('d/m/Y');
echo "
    <html>
        <style>
            .login {
                max-width: 500px;
                border: 1.5px solid #4e7aa3;
                margin: 0 auto;
                
            }
            .login_body {
                padding: 30px 60px;
            }
            .login_time {
                padding: 12px;
                background-color: #f2f2f2;
            }
            .login_form {
            }
            .form_name, .form_password {
                display: flex;
                margin-top: 20px;
            }
            .form-text {
                background-color: #5b9bd5;
                color: #fff;
                padding: 6px;
                width: 200px;
                margin-right: 20px;
                border: 1px solid #4e7aa3;
            }
            .form-input {
                border:1px solid #4e7aa3;
                flex: 1;
                outline:none;
            }
            .login_submit {
                margin-top: 30px;
                
                text-align: center;
            }
            .login-btn {
                background-color: #5b9bd5;
                color: #fff;
                border: 1.5px solid #4e7aa3;
                padding: 10px 20px;
                border-radius: 10px;
                cursor: pointer;
            }
        </style>

        <body>
        <div class='login'>
            <div class='login_body'>
                <div class='login_time'>Bây giờ là: $time $day</div>
                <div class='login_form'>
                    <div class='form_name'>
                        <div class='form-text'>Tên đăng nhập</div>
                        <input class='form-input' type='text'>
                    </div>
                    <div class='form_password'>
                        <div class='form-text'>Mật khẩu</div>
                        <input class='form-input' type='password'>
                    </div>
                </div>
                <div class='login_submit'>
                    <input class='login-btn' type='submit' value='Đăng nhập'>
                </div>
            </div>
        </div>
        </body>
        
    </html>" ;
?>